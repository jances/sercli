use std::time::Duration;

use tokio_serial::{Serial, SerialPort};

pub async fn establish_connection(path: &str, baudrate: u32) -> Result<Serial, std::io::Error> {
    let mut settings = tokio_serial::SerialPortSettings::default();
    settings.baud_rate = baudrate;
    settings.timeout = Duration::from_millis(200);
    let conn = tokio_serial::Serial::from_path(path, &settings);
    if let Ok(mut port) = conn {
        port.write_data_terminal_ready(false).unwrap();
        Ok(port)
    } else {
        eprintln!("Failed to connect to {}!", path);
        conn
    }
}
