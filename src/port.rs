#![allow(unused)]
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::sync::{mpsc, oneshot};
use tokio_serial::Serial;
use tokio_serial::SerialPort;

pub struct PortActor {
    receiver: mpsc::Receiver<PortMessage>,
    serial: Serial,
}

#[derive(Debug)]
pub enum PortMessage {
    Read {
        respond_to: oneshot::Sender<Result<(usize, String), std::io::Error>>,
    },
    Write {
        data_to_send: String,
        respond_to: oneshot::Sender<Result<(), std::io::Error>>,
    },
    Drop {
        respond_to: oneshot::Sender<Result<(), std::io::Error>>,
    },
}

impl PortActor {
    pub fn new(path: &str, baudrate: u32, receiver: mpsc::Receiver<PortMessage>) -> Self {
        let mut rt = tokio::runtime::Runtime::new().unwrap();
        PortActor {
            receiver,
            serial: rt
                .block_on(crate::utils::establish_connection(path, baudrate))
                .expect("Unable to connect to the port!"),
        }
    }

    pub fn from(serial: Serial, receiver: mpsc::Receiver<PortMessage>) -> Self {
        PortActor { receiver, serial }
    }

    async fn handle_message(&mut self, msg: PortMessage) {
        match msg {
            PortMessage::Read { respond_to } => {
                let mut buf = [0u8; 1024];
                let bytes = self.serial.bytes_to_read();
                match bytes {
                    Ok(s) => {
                        if s != 0 {
                            match self.serial.read(&mut buf).await {
                                Ok(len) => {
                                    //println!("Read {} bytes!", len);
                                    let data = String::from_utf8_lossy(&buf[..len]);
                                    let _ = respond_to.send(Ok((len, data.to_string())));
                                }
                                Err(e) => {
                                    let _ = respond_to.send(Err(e));
                                }
                            };
                        } else {
                            let _ = respond_to.send(Ok((0, String::new())));
                        }
                    }
                    Err(e) => {
                        let _ = respond_to.send(Ok((0, String::new())));
                    }
                }
            }
            PortMessage::Write {
                data_to_send,
                respond_to,
            } => {
                let _ = respond_to.send(self.serial.write_all(data_to_send.as_bytes()).await);
            }
            _ => {}
        }
    }
}

async fn run_my_actor(mut actor: PortActor) {
    while let Some(msg) = actor.receiver.recv().await {
        match msg {
            PortMessage::Drop { mut respond_to } => {
                respond_to.send(Ok(()));
                break;
            }
            _ => {}
        }
        actor.handle_message(msg).await;
    }
}

#[derive(Clone)]
pub struct PortHandle {
    sender: mpsc::Sender<PortMessage>,
}

impl PortHandle {
    pub fn new(port: &str, baudrate: u32) -> Self {
        let (sender, receiver) = mpsc::channel(8);
        let actor = PortActor::new(port, baudrate, receiver);
        tokio::spawn(run_my_actor(actor));

        PortHandle { sender }
    }

    pub fn from(port: Serial) -> Self {
        let (sender, receiver) = mpsc::channel(8);
        let actor = PortActor::from(port, receiver);
        tokio::spawn(run_my_actor(actor));

        PortHandle { sender }
    }

    pub async fn read(&mut self) -> Result<(usize, String), std::io::Error> {
        let (send, recv) = oneshot::channel();
        let msg = PortMessage::Read { respond_to: send };
        let _ = self.sender.send(msg).await;
        recv.await.expect("Actor task has been killed")
    }

    pub async fn write(&mut self, data_to_send: &str) -> Result<(), std::io::Error> {
        let (send, recv) = oneshot::channel();
        let msg = PortMessage::Write {
            data_to_send: data_to_send.to_string(),
            respond_to: send,
        };
        let _ = self.sender.send(msg).await;
        recv.await.expect("Actor task has been killed")
    }

    pub async fn drop(&mut self) -> Result<(), std::io::Error> {
        let (send, recv) = oneshot::channel();
        let msg = PortMessage::Drop { respond_to: send };
        let _ = self.sender.send(msg).await;
        recv.await.expect("Actor task has been killed!")
    }
}
