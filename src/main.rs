use crate::port::PortHandle;
use serde::{Deserialize, Serialize};
use std::thread::sleep;
use std::time::Duration;
use utils::establish_connection;
use web_view::*;

mod port;
mod utils;

#[derive(Serialize, Deserialize)]
struct Data {
    action: String,
    data: Option<String>,
    code: Option<String>,
    input_type: Option<String>,
    port: Option<String>,
    baudrate: Option<u32>,
}

#[tokio::main]
async fn main() {
    let html_content = include_str!("assets/index.html");
    let (sender, mut receiver) = tokio::sync::mpsc::channel::<String>(1024);
    let (conn_sender, mut conn_receiver) = tokio::sync::mpsc::channel::<Option<PortHandle>>(1);
    let view = web_view::builder()
        .title("SerCli | Serial Client written in Rust")
        .content(Content::Html(html_content))
        .size(800, 600)
        .resizable(true)
        .debug(true)
        .user_data(())
        .invoke_handler(|webview, arg| {
            //dbg!(arg);
            let data: Data = serde_json::from_str(arg).unwrap();
            match data.action.as_str() {
                "send_data" => {
                    let mut s = sender.clone();
                    tokio::spawn(async move { s.send(data.data.unwrap()).await });
                }
                "send_byte" => {
                    let mut s = sender.clone();
                    //tokio::spawn(async move {s.send(format!("{}", std::char::from_u32(data.code.unwrap()).unwrap())).await });
                    if let Some(code) = data.code{
                        tokio::spawn(async move {s.send(code).await });
                    }else{
                        if let Some(input_type) = data.input_type{
                            let string = match &input_type[..] {
                                "insertLineBreak" => {format!("\r\n")},
                                "deleteContentBackward" => {format!("\u{8}")},
                                _ => {panic!("Unknown operation `{}`!", input_type);}
                            };
                            tokio::spawn(async move{s.send(string).await});
                        }
                    }
                }
                "connect" => {
                    let mut s = conn_sender.clone();
                    tokio::spawn(async move {
                        println!(
                            "Connecting to the port {} with baudrate of {}.",
                            data.port.as_ref().unwrap(),
                            data.baudrate.unwrap()
                        );
                        match establish_connection(
                            data.port.as_ref().unwrap(),
                            data.baudrate.unwrap(),
                        )
                        .await
                        {
                            Ok(a) => {
                                if let Err(_) = s.send(Some(PortHandle::from(a))).await {
                                    eprintln!("Something went wrong while sending new PortHandle.");
                                } else {
                                    println!("Connected to the port {}.", data.port.unwrap());
                                }
                            }
                            Err(e) => {
                                eprintln!(
                                    "Connection to the port {} failed. {:#?}",
                                    data.port.unwrap(),
                                    e
                                );
                            }
                        }
                    });
                }
                "refresh_ports" => {
                    let ports = serialport::available_ports().unwrap();
                    webview
                        .eval(&format!(
                            r#"
                        var ports = document.getElementById("serial_ports");
                        ports.innerHTML = "";
                    "#
                        ))
                        .unwrap();
                    for f in ports {
                        webview
                            .eval(&format!(
                                r#"
                        var ports = document.getElementById("serial_ports");
                        var opt = document.createElement('option');
                        opt.value = '{}';
                        opt.innerHTML = '{}';
                        ports.appendChild(opt);
                        "#,
                                f.port_name, f.port_name
                            ))
                            .unwrap();
                    }
                    println!("Refreshing ports.");
                }
                "disconnect" => {
                    let mut s = conn_sender.clone();
                    tokio::spawn(async move {
                        // dbg!("#3");
                        // port.drop().await.unwrap();
                        if let Err(_) = s.send(None).await {
                            eprintln!("Something went wrong while sending None PortHandle.");
                        } else {
                            println!("Disconnected from the port {}.", data.port.unwrap());
                        }
                        // dbg!("#4");
                    });
                }
                _ => panic!("Unknown command!"),
            }
            Ok(())
        })
        .build()
        .unwrap();
    let handle = view.handle();
    tokio::spawn(async move {
        let mut ports: Option<PortHandle> = None;
        loop {
            if let Ok(a) = conn_receiver.try_recv() {
                if let Some(mut port) = ports {
                    port.drop().await.unwrap();
                }
                ports = a;
            }
            if let Some(port) = ports.as_mut() {
                if let Ok(l) = receiver.try_recv() {
                    if let Err(e) = port.write(&parse_string_to_send(&l)).await {
                        eprintln!("Failed to write to the port: {}", e);
                    }
                }
                match port.read().await {
                    Ok(a) => {
                        if a.0 > 0 {
                            let command = if a.1 == "\u{8} \u{8}" {
                                //println!("Backspace");
                                format!(
                                    r#"
                                var textarea = document.getElementById("serial_output");
                                textarea.value = textarea.value.substring(0, textarea.value.length - 1);
                                textarea.scrollTop = textarea.scrollHeight;"#
                                )
                            }else{
                                //println!("Data: {:#?}", a.1);
                                format!(
                                    r#"
                                var textarea = document.getElementById("serial_output");
                                textarea.value += {:#?};
                                textarea.scrollTop = textarea.scrollHeight;"#,
                                    a.1
                                )
                            };
                            handle
                                .dispatch(move |webview| {
                                    webview.eval(&command).unwrap();
                                    Ok(())
                                })
                                .unwrap();
                        }
                    }
                    Err(e) => {
                        eprintln!("Failed to read data: {}", e);
                    }
                }
            } else {
                sleep(Duration::from_millis(100));
            }
        }
    });
    view.run().unwrap();
}

fn parse_string_to_send(data: &str) -> String {
    let data = data.replace("\\r", "\r").replace("\\n", "\n");
    data
}
